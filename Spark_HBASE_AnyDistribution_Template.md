﻿# Spark-Hbase with any version of Spark

Note (to work with pre 2x Spark you need to replace ```spark.sparkContext``` with ``sc``)

(Should work for hortonworks, has been tested on Cloudera Clusters and is working)

You need to provide some hbase-dependencies. You should be able to find them using ```locate``` etc linux methods. My cloudera defaults were as follows 
```
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/hbase-server-1.2.0-cdh5.13.2.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/hbase-hadoop2-compat-1.2.0-cdh5.13.2.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/bin/../lib/metrics-core-2.2.0.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/bin/../lib/hbase-annotations-1.2.0-cdh5.13.2.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/bin/../lib/protobuf-java-2.5.0.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/bin/../lib/htrace-core.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/lib/hbase-client-1.2.0-cdh5.13.2.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/lib/hbase-common-1.2.0-cdh5.13.2.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/hbase-server-1.2.0-cdh5.13.2.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/lib/hbase-protocol-1.2.0-cdh5.13.2.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hadoop/hadoop-common-2.6.0-cdh5.13.2.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/hbase-hadoop-compat-1.2.0-cdh5.13.2.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hadoop/client/hadoop-mapreduce-client-jobclient-2.6.0-cdh5.13.2.jar
/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hadoop-hdfs/hadoop-hdfs-2.6.0-cdh5.13.2.jar
```
## # To load them with the spark-shell or spark-submit. 
```scala
 spark2-shell --jars /opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/hbase-server-1.2.0-cdh5.13.2.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/hbase-hadoop2-compat-1.2.0-cdh5.13.2.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/bin/../lib/metrics-core-2.2.0.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/bin/../lib/hbase-annotations-1.2.0-cdh5.13.2.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/bin/../lib/protobuf-java-2.5.0.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/bin/../lib/htrace-core.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/lib/hbase-client-1.2.0-cdh5.13.2.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/lib/hbase-common-1.2.0-cdh5.13.2.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/hbase-server-1.2.0-cdh5.13.2.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/lib/hbase-protocol-1.2.0-cdh5.13.2.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hadoop/hadoop-common-2.6.0-cdh5.13.2.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hbase/hbase-hadoop-compat-1.2.0-cdh5.13.2.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hadoop/client/hadoop-mapreduce-client-jobclient-2.6.0-cdh5.13.2.jar,/opt/cloudera/parcels/CDH-5.13.2-1.cdh5.13.2.p0.3/lib/hadoop-hdfs/hadoop-hdfs-2.6.0-cdh5.13.2.jar
```
# The Script 
```scala
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client._
import scala.util.Try
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.client.{Result, Scan}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp
import org.apache.hadoop.hbase.filter.{BinaryComparator, FilterList, MultipleColumnPrefixFilter, QualifierFilter}
import org.apache.hadoop.hbase.{Cell, CellUtil, HBaseConfiguration, TableName}
```
```scala
@transient val conf = HBaseConfiguration.create
conf.set("hbase.zookeeper.quorum", "your_endpoint1.net,server.yourenpoint2.net,server.your_endpoint3.net")
conf.set("hbase.zookeeper.property.clientPort", "2181") //default, should confirm
conf.set("hbase.client.scanner.timeout.period", "600000")
conf.set("hbase.rpc.timeout", "600000")
```
These will depend on your configuration 
```scala
conf.set("hadoop.ssl.enabled", "true")
conf.set("hbase.cluster.distributed", "true")
```
The following is needed if you are configuring with kerberos
```scala
conf.set("hadoop.security.authentication", "kerberos")
conf.set("hbase.security.authentication", "kerberos")
conf.set("hbase.master.kerberos.principal", "hbase/_HOST@your.NET")
conf.set("hbase.regionserver.kerberos.principal", "hbase/_HOST@your.NET")
```
Specifying your table scan 
```scala
import org.apache.hadoop.hbase.client.Scan
val limit = 1
val scan = new Scan()
scan.setMaxResultSize(limit)
```

Converting the Scan to the Base64 encoded string
```
import org.apache.hadoop.hbase.protobuf.ProtobufUtil
import org.apache.hadoop.hbase.util.Base64

def convertScanToString(scan: Scan): String = {
  val proto = ProtobufUtil.toScan(scan)
  Base64.encodeBytes(proto.toByteArray())
}
```
```scala
val stringScan = convertScanToString(scan)
```
Specifying a table to load 
```scala
import org.apache.hadoop.hbase.mapreduce.TableInputFormat

val tableName="db:yourtable"
conf.set(TableInputFormat.INPUT_TABLE, tableNamei)
conf.set(TableInputFormat.SCAN, stringScan)
```
Loading the table into an RDD

```scala
import org.apache.spark.rdd.NewHadoopRDD
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.client.Result

val hbaseRdd = spark.sparkContext.newAPIHadoopRDD(conf,
                                                  classOf[TableInputFormat],
                                                  classOf[ImmutableBytesWritable],
                                                  classOf[Result]
                                                 ).cache()
```

## Congrats! You now have your hbase table in an RDD. 
I would define a ```case class``` and use ```hbaseRdd.as[your_class]``` to convert it to a Dataset for operations. It will:
1) provide a clear and concise entry point to all of you operations/manipulations for others later on. 
2) provide you all of the dataframe/dataset benefits. 
